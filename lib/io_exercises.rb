# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  number = (rand * 100 + 1).floor
  num_guesses = 1

  print "Guess a number between 1 and 100 inclusive: "
  guess = gets.chomp.to_i

  while guess != number
    puts guess < number ? "#{guess} is too low" : "#{guess} is too high"
    puts guess

    print "Guess a number between 1 and 100 inclusive: "
    guess = gets.chomp.to_i

    num_guesses += 1
  end

  puts "Game over. Number of guesses was #{num_guesses}"
end

# guessing_game
