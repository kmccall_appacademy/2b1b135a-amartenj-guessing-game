# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def shuffle_file(filename)
  puts filename

  contents = File.readlines("#{filename}.txt")
  contents.last << "\n"

  contents.shuffle!
  last_line = contents.length - 1

  f = File.open("#{filename}-shuffled.txt", "w")
  contents.each.with_index { |line, idx| idx == last_line ? f << line.chomp : f << line }
  f.close
end

if __FILE__ == $PROGRAM_NAME
  shuffle_file(ARGV[0])
end
